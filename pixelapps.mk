# Copyright (C) 2018 The Pixel Dust Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleCamera \
    GoogleVrCore \
    LatinIMEGooglePrebuilt \
    NexusWallpapersStubPrebuilt2017 \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    Tycho \
    talkback \
    WallpapersBReel2017

# framework
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2016 \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects

# priv-app
PRODUCT_PACKAGES += \
    GCS \
    GoogleContacts \
    GoogleDialer \
    NexusLauncherPrebuilt \
    StorageManagerGoogle \
    Turbo \
    WallpaperPickerGooglePrebuilt

# Libraries
PRODUCT_COPY_FILES += \
    vendor/proprietary_pixel/lib64/libgdx.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libgdx.so \
    vendor/proprietary_pixel/lib64/libwallpapers-breel-jni.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libwallpapers-breel-jni.so


# Symlinks
PRODUCT_PACKAGES += \
    libgdx.so \
    libwallpapers-breel-jni.so \
    libjpeg.so

# Blobs
PRODUCT_COPY_FILES += \
    vendor/proprietary_pixel/etc/permissions/com.google.android.camera.experimental2016.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.camera.experimental2016.xml \
    vendor/proprietary_pixel/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/proprietary_pixel/etc/permissions/com.google.android.maps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.maps.xml \
    vendor/proprietary_pixel/etc/permissions/com.google.android.media.effects.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.media.effects.xml \
    vendor/proprietary_pixel/etc/default-permissions/default-permissions.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default-permissions/default-permissions.xml \
    vendor/proprietary_pixel/etc/permissions/privapp-permissions-googleapps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-googleapps.xml \
    vendor/proprietary_pixel/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/preferred-apps/google.xml \
    vendor/proprietary_pixel/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google.xml \
    vendor/proprietary_pixel/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_build.xml \
    vendor/proprietary_pixel/etc/sysconfig/google_vr_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_vr_build.xml \
    vendor/proprietary_pixel/etc/sysconfig/pixel_2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2017.xml \
    vendor/proprietary_pixel/etc/sysconfig/pixel_2017_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2017_exclusive.xml
